package main

import (
    "fmt"
    "bytes"
    "time"
    "strings"
    "strconv"
    "math/rand"
    "net/http"

    dt "github.com/user/net_vis/data_types"

    "github.com/kandoo/beehive"
)



// NetVis connection variables
var url = "http://localhost:5000/messages"
var client = &http.Client{}

type Receiver struct{}

func (r *Receiver) Map(msg beehive.Msg, ctx beehive.MapContext) beehive.MappedCells {
    return beehive.MappedCells{{"ReceiverDict", "0"}}
}

// Receives the message and the context.
func (r *Receiver) Rcv(msg beehive.Msg, ctx beehive.RcvContext) error {
    // msg is an envelope around the Hello message.
    // You can retrieve the Message using msg.Data() and then
    // you need to assert that its a Message.
    message := msg.Data().(dt.Message)

    // Get or create the dictionary
    dict := ctx.Dict("test_dict")

    // Attempt to get the count from the dictionary
    v, err := dict.Get(message.AppId)

    // If there is an error, the entry is not in the
    // dictionary. Otherwise, we set count based on
    // the value we already have in the dictionary
    // for that name.
    count := 0
    if err == nil {
        count = v.(int)
    }

    // Now we increment the count.
    count++

    // And then we print the hello message.
    ctx.Printf("Message received from App #%s: %s, copies=%s (%d)!\n", message.AppId, message.Contents, message.Copies, count)


    // Send the messages to the NetVis server
    fmt.Println("Passing message on to server")
    app_id := `"app_id":"` + message.AppId + `"`
    contents := `"contents":"` + message.Contents + `"`
    copies := `"copies":"` + message.Copies + `"`
    jsonStr := []byte(`{` + app_id + `, ` + contents + `, ` + copies + `}`)
    req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
    req.Header.Set("Content-Type", "application/json")

    resp, err := client.Do(req)
    if err != nil {
        panic(err)
    }
    defer resp.Body.Close()


    // Finally we update the count stored in the dictionary.
    return dict.Put(message.AppId, count)
}

func emitMessages() {
    // Emit a random length message
    base_msg := "Hello World!"
    // Seed the PRNG
    rand.Seed(time.Now().UnixNano())
    for {
        // Generate random number of copies
        copies := rand.Intn(9) + 1
        msg := strings.Repeat(base_msg, copies)

        fmt.Println("Emitting message")
        beehive.Emit(dt.Message{AppId: "1", Contents:msg, Copies:strconv.Itoa(copies)})

        // Sleep for 1s
        time.Sleep(1 * time.Second)
    }
}

func main() {
    go emitMessages()

    // Create the receiver application
    app := beehive.NewApp("test-receiver")

    // Register the Message handler
    app.Handle(dt.Message{}, &Receiver{})


    fmt.Println("Now listening for messages")

    beehive.Start()
}
