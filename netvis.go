package main

import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"github.com/user/net_vis/data_types"
	"golang.org/x/net/websocket"
	"gopkg.in/gorp.v1"

	"log"
	"net/http"
)

var (
	dbmap = initDb()
)

func main() {

	// Serve web page and associated resources for front end
	http.Handle("/dist", http.FileServer(http.Dir("./front_end/dist")))
	http.Handle("/", http.FileServer(http.Dir("./front_end")))
	// Expose web socket to send data to front end
	http.Handle("/api", websocket.Handler(socketHandler))

	http.HandleFunc("/messages", messagesHandler)
	http.HandleFunc("/registerType", registerTypeHandler)
	http.HandleFunc("/customMessages", customMessagesHandler)

	log.Fatal(http.ListenAndServe(":5000", nil))
}


func checkErr(err error, msg string) {
	if err != nil {
		log.Fatal(msg, err)
	}
}

// Initializes database connection and create necessary tables
// Returns connection to database
func initDb() *gorp.DbMap {
	// My .sock file is in a different location, we need a proper way to load, maybe a config file or something
	//db, err := sql.Open("mysql", "root:Inter123@unix(/opt/local/var/run/mysql56/mysqld.sock)/net_vis")
	db, err := sql.Open("mysql", "root:Inter123@unix(/run/mysqld/mysqld.sock)/net_vis")
	checkErr(err, "sql.open failed ")
	dbmap := &gorp.DbMap{Db: db, Dialect: gorp.MySQLDialect{"InnoDB", "UTF8"}}
	dbmap.AddTableWithName(data_types.TestData{}, "test_data").SetKeys(true, "Id")
	dbmap.AddTableWithName(data_types.User{}, "users").SetKeys(true, "Id")
	dbmap.AddTableWithName(data_types.Message{}, "Messages").SetKeys(true, "MessageId")
	err = dbmap.CreateTablesIfNotExists()
	checkErr(err, "Create table failed ")

	return dbmap
}
