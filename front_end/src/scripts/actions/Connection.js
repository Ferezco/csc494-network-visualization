import uuid from '../utils/uuid';

export default class Connection {

    constructor(apiendpoint) {
        this.socket = new WebSocket(apiendpoint)
        this.socket.onopen = this.openConnection.bind(this)
        this.socket.onmessage = this.handleMessage.bind(this)
        this.sendData = this.sendData.bind(this)
        this.handlers = {}
        this.login = this.login.bind(this)
        this.signUp = this.signup.bind(this)
    }

    openConnection() {
        this.uid = null
        console.log('Connection: Open')
    }

    handleMessage(event) {
        var data = JSON.parse(event.data)
        console.log('recieved', data)

        if (this.handlers.hasOwnProperty(data.type)) {
            this.handlers[data.type](data.contents)
        } else {
            console.log('fail', event)
        }
    }

    sendData(type, data) {
        console.log('Connection: sendData', data)
        this.socket.send(JSON.stringify({type: type, uid: this.uid, contents: data}))
    }

    login(user, pass, cb) {
        this.handlers['login'] = (contents) => {
            var ok = contents['error'] || false;
            if (!ok)
                this.user = {user: {uid: contents.uid, user: contents.user}};
            cb(ok);
        }

        this.sendData('login', {user: user, pass: pass});
    }

    signup(user, pass, cb) {
        this.handlers['signup'] = (contents) => {
            cb(contents['error'] || false);
        }

        this.sendData('signup', {user: user, pass: pass})
    }
}
