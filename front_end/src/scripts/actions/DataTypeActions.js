'use strict';

import { Action } from 'geiger';
import axios from 'axios';


export default class DataTypeActions extends Action {

    constructor({ socket }) {
        super();
        
    }

    fetchMessages() {
        return axios
            .get(this.apiendpoint)
            .then(msgs => msgs.data.map(m => {
                m.date = new Date(m.date)
                return new MessageRecord(m)
            })).then(msgs => this.emit('fetchMessages', msgs));
    }
}
