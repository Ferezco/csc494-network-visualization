'use strict';

import { Action } from 'geiger';

import { DataRecord } from '../records';
import uuid from '../utils/uuid';

export default class TestDataActions extends Action {

    constructor({ connection }) {
        super();
        connection.handlers['data'] = this.recieveData.bind(this)
    }

    recieveData(contents) {
        var data = contents.map(d => {
            d.date = new Date(d.date * 1000)
            return new DataRecord(d)
        });

        this.emit('newData', data);

        // return axios
        //     .get(this.apiendpoint)
        //     .then(msgs => msgs.data.map(m => {
        //         m.date = new Date(m.date)
        //         return new MessageRecord(m)
        //         }))  // passed to the store after REST response (obviously); sliced for the demo
        //     .then(msgs => this.emit('fetchMessages', msgs));
    }

    createData() {

    }
}
