'use strict';

import React from 'react';
import {render} from 'react-dom';
import {Router, Route, IndexRoute, Link, hashHistory} from 'react-router';

import Connection from '../actions/Connection';
import DataTypeStore from '../stores/DataTypeStore';
import DataTypeActions from '../actions/DataTypeActions';

import {Login, SignUp} from './UI/UserInfo';
import Home from './UI/Home';
import NavBar from './UI/NavBar';
import DashBoard from './UI/DashBoard';
import auth from '../utils/auth';

class App extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            connection: new Connection("ws://localhost:5000/api")
        }
    }

    getChildContext() {
        return this.state
    }

    render() {
        return (
            <div id="wrapper">
                {this.props.children}
            </div>
        );
    }
}

App.childContextTypes = {
    connection: React.PropTypes.object.isRequired
}

render((
    <Router history={hashHistory}>
        <Route path="/" component={App}>
            <IndexRoute component={Login} />
            <Route path="signUp" component={SignUp} />
            <Route path="home/" component={Home}>
                <IndexRoute component={DashBoard}/>
            </Route>
        </Route>
    </Router>
), document.getElementById('app'));
