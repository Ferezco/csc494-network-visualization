'use strict';

import React from 'react';
import {browserHistory} from 'react-router';
import App from '../main';


class UserInfo extends React.Component {

    constructor(props) {
        super(props);
        console.log(props)
        this.state = {alerts: false};
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    handleSubmit(event) {
        event.preventDefault();
        this.context.connection[this.props.cb](this.state.user, this.state.pass, (ok) => {
            if (ok)
                return this.setState({alerts: ok})

            this.context.router.push(this.props.redirect)
        });
    }

    render() {
        return (
            <div id="page-wrapper">
                <div className="container-fluid">
                    <div className="col-lg-4"></div>
                    <div className="col-lg-4 jumbotron">
                        <div className="container">
                            <h1>{this.props.title}</h1>
                            {this.state.alerts && <div className="alert alert-info">{this.state.alerts}</div>}
                            <form role="form" onSubmit={this.handleSubmit}>
                                <div className="form-group">
                                    <label>User Name:</label>
                                    <input type="text" className="form-control" onChange={(event) => {
                                        this.setState({user: event.target.value})
                                    }}/>
                                </div>
                                <div className="form-group">
                                    <label>Password</label>
                                    <input type="password" className="form-control" onChange={(event) => {
                                        this.setState({pass: event.target.value})
                                    }}/>
                                </div>
                                <button className="btn btn-primary btn-block" type="submit">{this.props.title}</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

UserInfo.contextTypes = {
    router: React.PropTypes.object.isRequired,
    connection: React.PropTypes.object.isRequired
}

UserInfo.PropTypes = {
    title: React.PropTypes.string.isRequired,
    cb: React.PropTypes.string.isRequired,
    redirect: React.PropTypes.string.isRequired
}

export class Login extends React.Component {
    render() {
        return (
            <div>
                <UserInfo title="Login" cb="login" redirect="/home"/>
            </div>
        );
    }
}

export class SignUp extends React.Component {
    render() {
        return (
            <div>
                <UserInfo title="Sign Up" cb="signup" redirect="/"/>
            </div>
        );
    }
}
