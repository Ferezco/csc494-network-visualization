import React from 'react/';
import LineChart from 'react-d3/linechart/LineChart';

export default class Chart extends React.Component {

    componentWillMount() {
        this.unwatchStore = this.context.store.watch(this.forceUpdate.bind(this));
    }

    componentWillUnmount() { this.unwatchStore(); }

    render() {
        var data = this.context.store.getData()
        // var data = messages.map(msg => {return { x: msg.get('x'), y: msg.get('y')}})
        // console.log(data[0])
        console.log(data)
        return (
            <div className="container-fluid">
                <div className="row">
                    <div className="col-lg-12">
                        <h1 className="page-header">
                            Dashboard <small>Statistics Overview</small>
                        </h1>
                    </div>
                </div>
                <div className="row">
                    {data.length != 0 && <LineChart legend={false} data={[{
                                    name: 'Messages',
                                    values: data
                                }
                            ]} width={800} height={800} viewBoxObject={{
                                x: 0,
                                y: 0,
                                width: 200,
                                height: 200
                            }} yAxisLabel="Number" xAxisLabel="Elapsed Time (sec)" gridHorizontal={true}/>

                    }
                </div>

            </div>
        );
    }
}
Chart.contextTypes = {
    store: React.PropTypes.object.isRequired,
    actions: React.PropTypes.object.isRequired,
    connection: React.PropTypes.object.isRequired
}

// this.props.store.getMessages().map(msg => {return { x: msg.date.getSeconds(), y: 5}})
