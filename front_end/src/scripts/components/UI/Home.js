'use strict';

import React from 'react/';
import NavBar from './NavBar'

import TestDataActions from '../../actions/TestDataActions';
import TestDataStore from '../../stores/TestDataStore';

export default class Home extends React.Component {

    constructor(props, context) {
        super(props, context);
        var actions = new TestDataActions({ connection: context.connection });

        this.state = {
            actions: actions,
            store: new TestDataStore({ actions: actions })
        }
    }

    getChildContext() {
        return this.state;
    }

    render() {
        return (
            <div>
                <NavBar />
                <div id="page-wrapper">
                    {this.props.children}
                </div>
            </div>
        );
    }
}

Home.contextTypes = {
    router: React.PropTypes.object.isRequired,
    connection: React.PropTypes.object.isRequired,
    user: React.PropTypes.object
}

Home.childContextTypes = {
    actions: React.PropTypes.object.isRequired,
    store: React.PropTypes.object.isRequired
}
