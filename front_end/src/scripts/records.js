'use strict';

import Immutable from 'immutable';

export class DataRecord extends Immutable.Record({
    id: null,
    date: null,
    x: null,
    y: null,
}) {
    label() { return this.get('id'); }
}
