'use strict';

import { Store } from 'geiger';
import Immutable from 'immutable';

export default class TestDataStore extends Store {

    constructor({ actions }) {

        super();
        this.data = Immutable.OrderedMap();

        /*
        * Registering action handlers
        * Intentionnaly made private (just use the actions !)
        */

        this.listen(actions, 'newData', data => {
            console.log(data)
            for(let d of data) {
                this.data = this.data.set(d.get('id'), d);
            }
            this.changed();
        });
        //
        // this.listen(actions, 'createMessage', msg => {
        //     this.messages = this.messages.set(msg.get('id'), msg);
        //     this.changed();
        // });
        //
        // this.listen(actions, 'deleteMessage', msg => {
        //     this.messages = this.messages.delete(msg.get('id'));
        //     this.changed();
        // });
    }

    getData() { return this.data.toArray(); }
}
