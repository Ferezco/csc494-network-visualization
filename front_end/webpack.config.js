'use strict'

var webpack = require('webpack');

module.exports = {
  entry: './src/scripts/components/main.js',
  output: {
    filename: 'bundle.js',
    path: './dist'
  },

  module : {
    loaders : [
      {
        test : /\.jsx?/,
        exclude: /node_modules/,
        loader : 'babel'
      }
    ]
  },

  cache: true,
  debug: true,
  devtools: false,
} 