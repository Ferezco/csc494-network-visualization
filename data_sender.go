package main

import (
	"net/http"
	"math/rand"
	"strings"
	"time"
	"strconv"
	"bytes"
	"fmt"
)

func main() {
	base_msg := "Hello World!"

	// Seed the PRNG
	rand.Seed(time.Now().UnixNano())

	url := "http://localhost:5000/messages"
	client := &http.Client{}

	for {
		// Generate random number of copies
		copies := rand.Intn(9) + 1
		msg := strings.Repeat(base_msg, copies)
		fmt.Println("Sending message: " + msg)

		// Send the message to the server
		var jsonStr = []byte(`{"app_id":"1", "contents":"` + msg + `", "copies":"` + strconv.Itoa(copies) + `"}`)
		req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
		req.Header.Set("Content-Type", "application/json")

		resp, err := client.Do(req)
		if err != nil {
			panic(err)
		}
		defer resp.Body.Close()

		// Sleep for 1s
		time.Sleep(1 * time.Second)
	}
}
