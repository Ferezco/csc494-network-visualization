package netvis_beehive

import (
  "fmt"

  "reflect"
  dt "github.com/user/net_vis/data_types"
  "encoding/json"
  "net/http"
  "bytes"

  bh "github.com/kandoo/beehive"
)


type Netvis struct{}
var netvis_instance = Netvis{}

var server_url = "http://localhost:5000"
var register_url = "/registerType"
var add_custom_msg_url = "/customMessages"
var client = &http.Client{}


func (nv *Netvis) Map(msg bh.Msg, ctx bh.MapContext) bh.MappedCells {
  return bh.MappedCells{{"NetvisDict", "1"}}
}

func (nv *Netvis) Rcv(msg bh.Msg, ctx bh.RcvContext) error {
  data := msg.Data()
  data_type := reflect.TypeOf(data)


  from_bee_id := msg.From()
  // TODO: Get the BeeInfo object based on the from_bee_id
  fmt.Printf("Is this a NoReply message from bee with ID %v? %v\n", from_bee_id, msg.NoReply())
  fmt.Printf("Attempt to get the BeeInfo object by ID: %+v", bh.DefaultHive.registry.bee(ctx.ID()))

  // TODO: Find the right way to obtain the name of the app that sent the message
  // ctx.App() will return the receiving app, not the sending app
  app_name := "HelloWorld"
  if data_type.Name() == "ping" {
    app_name = "Pinger"
  } else if data_type.Name() == "pong" {
    app_name = "Ponger"
  }

  fmt.Printf("Netvis ---> Received a message of type %v!\n", data_type.Name())
  fmt.Printf("Netvis ---> Message came from app %v\n", app_name)
  fmt.Printf("Netvis ---> Message contains following data: %+v\n", data)

  // REMOVE
  return nil

  // Send the custom message to the server, incorporating the app name, message type and custom fields
  app_name_json := `"app_name":"` + app_name + `"`
  message_type := `"message_type":"` + data_type.Name() + `"`

  field_data := extractFieldData(data_type, reflect.ValueOf(data))
  field_data_json, _ := json.Marshal(field_data)
  fmt.Printf("field_data_json: %v\n", string(field_data_json))

  fields := `"field_data":` + string(field_data_json)
  jsonStr := []byte(`{` + app_name_json + `,` + message_type + `,` + fields + `}`)

  // Generate the right request
  req, _ := http.NewRequest("POST", server_url + add_custom_msg_url, bytes.NewBuffer(jsonStr))
  req.Header.Set("Content-Type", "application/json")

  // Send the request to register the message with the DB
  resp, err := client.Do(req)
  if err != nil {
      panic(err)
  }
  defer resp.Body.Close()

  fmt.Println()
  return nil
}


func extractFieldData(data_type reflect.Type, data_value reflect.Value) []dt.FieldData {
  field_data := make([]dt.FieldData, data_type.NumField())

  for i := 0; i < len(field_data); i++ {
    name := data_type.Field(i).Name
    data := data_value.Field(i)

    field_data[i] = dt.FieldData{Name:name, Data:fmt.Sprintf("%v",data)}
  }

  return field_data
}

func extractFields(data_type reflect.Type) []dt.Field {
  data_fields := make([]dt.Field, data_type.NumField())
  for i := 0; i < len(data_fields); i++ {
    field := data_type.Field(i)

    data_fields[i] = dt.Field{Name:field.Name, Type:field.Type.Name()}
  }

  return data_fields
}



func NetvisRegister(netvisApp bh.App, msg_type interface{}, app_name string) {
  fmt.Printf("Netvis ---> Registering fields for app with name: \"%v\"\n", app_name)
  fmt.Printf("Netvis ---> Registering new type called: %T\n", msg_type)

  specific_type := reflect.TypeOf(msg_type)
  data_fields := extractFields(specific_type)

  fmt.Printf("Netvis ---> Type contains the following fields: %+v\n", data_fields)

  // REMOVE
  fmt.Println()
  netvisApp.Handle(msg_type, &netvis_instance)
  return

  // Send the messages to the NetVis server
  fmt.Println("Sending register message to server")

  // Generate app name, the new message type and the fields of that message type for JSON sending
  app_name_json := `"app_name":"` + app_name + `"`
  message_type := `"message_type":"` + specific_type.Name() + `"`
  data_fields_json, _ := json.Marshal(data_fields)
  fields := `"fields":` + string(data_fields_json)
  jsonStr := []byte(`{` + app_name_json + `,` + message_type + `,` + fields + `}`)

  // Generate the right request
  req, err := http.NewRequest("POST", server_url + register_url, bytes.NewBuffer(jsonStr))
  req.Header.Set("Content-Type", "application/json")

  // Send the request to register the message with the DB
  resp, err := client.Do(req)
  if err != nil {
      panic(err)
  }
  defer resp.Body.Close()


  fmt.Println()
  netvisApp.Handle(msg_type, &netvis_instance)
}

