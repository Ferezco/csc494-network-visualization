package main

/* Simply emits random copies of the string "Hello World!"  */


import (
    "fmt"
    "time"
    "strings"
    "strconv"
    "math/rand"

    nv "github.com/user/net_vis/netvis_beehive"

    "github.com/kandoo/beehive"
)


var app_name = "HelloWorld"

// Used for testing
type HelloWorld struct {
    Contents        string  `json:"contents" binding:"required"`
    Copies          string  `json:"copies" binding:"required"`
}

type Receiver struct{}

func (r *Receiver) Map(msg beehive.Msg, ctx beehive.MapContext) beehive.MappedCells {
    return beehive.MappedCells{{"ReceiverDict", "0"}}
}

// Receives the message and the context.
func (r *Receiver) Rcv(msg beehive.Msg, ctx beehive.RcvContext) error {
    message := msg.Data().(HelloWorld)

    dict := ctx.Dict("test_dict")

    v, err := dict.Get(app_name)

    count := 0
    if err == nil {
        count = v.(int)
    }

    // Now we increment the count.
    count++

    // And then we print the hello message.
    ctx.Printf("Message received from App \"%v\": %s, copies=%s (%d)!\n", app_name, message.Contents, message.Copies, count)



    // Finally we update the count stored in the dictionary.
    return dict.Put(app_name, count)
}

func emitMessages() {
    // Emit a random length message
    base_msg := "Hello World!"
    // Seed the PRNG
    rand.Seed(time.Now().UnixNano())
    for {
        // Generate random number of copies
        copies := rand.Intn(9) + 1
        msg := strings.Repeat(base_msg, copies)

        fmt.Println("Emitting message")
        beehive.Emit(HelloWorld{Contents:msg, Copies:strconv.Itoa(copies)})

        // Sleep for 1s
        time.Sleep(1 * time.Second)
    }
}

func main() {
    go emitMessages()

    // Create the receiver application
    helloWorldApp := beehive.NewApp("HelloWorld")

    // Register the Message handler
    helloWorldApp.Handle(HelloWorld{}, &Receiver{})


    // Currently using the app name as the unique identifier
    netvisApp := beehive.NewApp("NetVis")
    nv.NetvisRegister(netvisApp, HelloWorld{}, helloWorldApp.Name())


    fmt.Println("Now listening for messages")

    beehive.Start()
}
