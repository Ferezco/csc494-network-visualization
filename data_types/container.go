package data_types

import (
	"errors"
	"fmt"
	"reflect"
)

type (
	InputMessage struct {
		Type     string      `json:"type"`
		Uid      string      `json:"uid"`
		Contents interface{} `json:"contents"`
	}
	OutputMessage struct {
		Type     string      `json:"type"`
		Contents interface{} `json:"contents"`
	}
	User struct {
		Id       int64  `json:"uid"`
		UserName string `json:"user"`
		Pass     string `json:"-"`
	}
	Error struct {
		Error string `json:"error"`
	}
)

func SetField(obj interface{}, name string, value interface{}) error {
	structValue := reflect.ValueOf(obj).Elem()
	structFieldValue := structValue.FieldByName(name)

	if !structFieldValue.IsValid() {
		return fmt.Errorf("No such field: %s in obj", name)
	}

	if !structFieldValue.CanSet() {
		return fmt.Errorf("Cannot set %s field value", name)
	}

	structFieldType := structFieldValue.Type()
	val := reflect.ValueOf(value)
	if structFieldType != val.Type() {
		return errors.New("Provided value type didn't match obj field type")
	}

	structFieldValue.Set(val)
	return nil
}
