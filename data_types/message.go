package data_types

import "fmt"

// Was used for testing, will be removed
type Message struct {
	MessageId int64
	AppId     string `json:"app_id" binding:"required"`
	Contents  string `json:"contents" binding:"required"`
	Copies    string `json:"copies" binding:"required"`
}

// Custom message sent from the NetVis Beehive app to the server to add information
type CustomMessage struct {
	AppName 			string 				`json:"app_name" binding:"required"`
	MessageType 	string 				`json:"message_type" binding:"required"`
	FieldData 		[]FieldData 	`json:"field_data" binding:"required"`
}

// Message sent from the NetVis Beehive app to the server to register a new data type to track
// Soheil confirmed that app names are unique; creating a second app with the same name as another one overwrites it
type RegisterTypeMessage struct {
	AppName 			string 		`json:"app_name" binding:"required"`
	MessageType 	string 		`json:"message_type" binding:"required"`
	Fields 				[]Field 	`json:"fields" binding:"required"`
}

// Type used for the custom data in the message from the NetVis Beehive app to the server
type FieldData struct {
	Name string `json:"field_name" binding:"required"`
	// For now, send all data as strings
	Data string `json:"field_data" binding:"required"`
}

// Represents the names and types of the fields of the custom data type to track
type Field struct {
	Name string `json:"name" binding:"required"`
	Type string `json:"type" binding:"required"`
}

// Type for retrieving X-Y data from any generic custom type table
type DBRowset struct {
	XCol string
	YCol string
}

type TestData struct {
	Id   int64 `json:"id"`
	Date int64 `json:"date"`
	X    int64 `json:"x"`
	Y    int64 `json:"y"`
}

func (s *TestData) FillStruct(m map[string]interface{}) error {
	for k, v := range m {
		err := SetField(s, k, v)
		if err != nil {
			return err
		}
	}
	return nil
}

func (m *TestData) String() string {
	return fmt.Sprintf("Message From %d to %d", m.X, m.Y)
}
