package main

import (
	"encoding/json"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/user/net_vis/data_types"
	"net/http"
)


func customMessagesHandler(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
		case "GET":
			getCustomMessages(w, r)
		case "POST":
			postCustomMessage(w, r)
		default:
			http.Error(w, "Unrecognized request", http.StatusInternalServerError)
	}
}

func postCustomMessage(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Post for custom message arrived")

	decoder := json.NewDecoder(r.Body)
  var m data_types.CustomMessage
  err := decoder.Decode(&m)
  if err != nil {
      http.Error(w, "Could not parse JSON fields", http.StatusInternalServerError)
  }

	if m.AppName != "" && m.MessageType != "" {
		fmt.Println("Received custom message from the Beehive app!")
		fmt.Printf("%+v\n", m)

		// Get the table name
		table_name := "App_" + m.AppName + "_" + m.MessageType

		// Get the column names and column values
		column_names := ""
		question_marks := ""
		column_values := make([]interface{}, len(m.FieldData))
		for i, fd := range m.FieldData {
			if i != 0 {
				column_names += ", "
				question_marks += ", "
			}

			column_names += fd.Name
			question_marks += "?"

			column_values[i] = fd.Data
		}


		insert_str := fmt.Sprintf("INSERT INTO %v (%v) VALUES (%v)", table_name, column_names, question_marks)
		fmt.Printf("%v %v\n", insert_str, column_values)
		insert, dberr := dbmap.Exec(insert_str, column_values...)


		if insert != nil && dberr == nil {
			if message_id, err := insert.LastInsertId(); err == nil {
				fmt.Printf("Inserted new custom message with message_id = %d\n", message_id)
			} else {
				fmt.Printf("Insert failed: %v\n", err)
			}
		} else {
			fmt.Printf("Insert failed: %v\n", dberr)
			checkErr(err, "Insert Failed")
		}

	} else {
		http.Error(w, "Some fields are empty", http.StatusTeapot)
	}
}

func getCustomMessages(w http.ResponseWriter, r *http.Request) {
	app_name := r.URL.Query().Get("AppName")
	message_type := r.URL.Query().Get("MessageType")
	y_column_name := r.URL.Query().Get("YCol")

	var m []data_types.DBRowset

	// Get the table name
	table_name := "App_" + app_name + "_" + message_type

	_, err := dbmap.Select(&m, fmt.Sprintf("SELECT TIME_ADDED AS XCol, %v AS YCol FROM %v" , y_column_name, table_name))


	if err == nil {
		js, err := json.Marshal(m)
		if err != nil {
	    http.Error(w, err.Error(), http.StatusInternalServerError)
	    return
	  }

	  w.Header().Set("Content-Type", "application/json")
	  w.Write(js)

	} else {
		err_msg := fmt.Sprintf("Failed to retrieve messages for App \"%v\", message type \"%v\", field \"%v\"\n",
														app_name, message_type, y_column_name)
		fmt.Println(err)
		http.Error(w, err_msg, http.StatusInternalServerError)

	}
}

func registerTypeHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Register message arrived")

	decoder := json.NewDecoder(r.Body)

  var m data_types.RegisterTypeMessage
  err := decoder.Decode(&m)
  if err != nil {
      http.Error(w, "Could not parse JSON fields", http.StatusInternalServerError)
  }

  fmt.Printf("Successfully decoded register message: %+v\n", m)

  // Prepare the table name and the column types
  table_name := "App_" + m.AppName + "_" + m.MessageType

  // Convert the Field types to appropriate database types
  create_fields := ""
  for i,field := range m.Fields {
  	// Change the type to something MySQL recognizes
  	switch field.Type {
  		case "int":
  			field.Type = "INTEGER"
  		case "string":
  			field.Type = "TEXT"
  	}

  	// Add comma to chain the fields together
  	if i != 0 {
  		create_fields += ", "
  	}

  	// Combine to get the full column definition; all columns are non-nullable [subject to change]
  	create_fields += field.Name + " " + field.Type + " NOT NULL"
  }

  _, err = dbmap.Exec("CREATE TABLE IF NOT EXISTS " + table_name + "( " +
  										"MESSAGE_ID int NOT NULL AUTO_INCREMENT, " +
  										"TIME_ADDED TIMESTAMP DEFAULT CURRENT_TIMESTAMP," +
  										create_fields +
  										", PRIMARY KEY (MESSAGE_ID)" +
  										" )")

  if err != nil {
    http.Error(w, "Could not create table "  + table_name, http.StatusInternalServerError)
  }
}



func messagesHandler(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
		case "GET":
			getMessages(w, r)
		case "POST":
			postMessage(w, r)
		default:
			http.Error(w, "Unrecognized request", http.StatusInternalServerError)
	}
}

func postMessage(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Post message arrived")

	decoder := json.NewDecoder(r.Body)
  var m data_types.Message
  err := decoder.Decode(&m)
  if err != nil {
      http.Error(w, "Could not parse JSON fields", http.StatusInternalServerError)
  }

	if m.AppId != "" && m.Contents != "" && m.Copies != "" {
		fmt.Println("Received nonempty message from the Beehive app!")
		fmt.Printf("%+v\n", m)

		insert, dberr := dbmap.Exec(`INSERT INTO Messages (AppId, Contents, Copies) VALUES (?, ?, ?)`, m.AppId, m.Contents, m.Copies)
		if insert != nil && dberr == nil {
			if message_id, err := insert.LastInsertId(); err == nil {
				fmt.Printf("Inserted new message with message_id = %d\n", message_id)
			} else {
				fmt.Printf("Insert failed: %v\n", err)
			}
		} else {
			fmt.Printf("Insert failed: %v\n", dberr)
			checkErr(err, "Insert Failed")
		}

	} else {
		http.Error(w, "Some fields are empty", http.StatusTeapot)
	}
}


func getMessages(w http.ResponseWriter, r *http.Request) {
	app_id := r.URL.Query().Get("AppId")

	var m []data_types.Message
	var err error
	if app_id != "" {
		_, err = dbmap.Select(&m, "SELECT * FROM Messages WHERE AppId=?", app_id)
	} else {
		_, err = dbmap.Select(&m, "SELECT * FROM Messages")
	}


	if err == nil {
		js, err := json.Marshal(m)
		if err != nil {
	    http.Error(w, err.Error(), http.StatusInternalServerError)
	    return
	  }

	  w.Header().Set("Content-Type", "application/json")
	  w.Write(js)

	} else {
		err_msg := "Failed to retrieve messages"
		if app_id != "" {
			err_msg += fmt.Sprintf(" for AppId=%v\n", app_id, err)
		}

		fmt.Printf("Error: %v\n", err)
		http.Error(w, err_msg, http.StatusInternalServerError)

	}
}
