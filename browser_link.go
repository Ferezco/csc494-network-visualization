package main

import (
	"encoding/json"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/user/net_vis/data_types"
	"golang.org/x/crypto/bcrypt"
	"golang.org/x/net/websocket"
)

func socketHandler(ws *websocket.Conn) {

	for {
		var t data_types.InputMessage
		var msg string

		if err := websocket.Message.Receive(ws, &msg); err != nil {
			fmt.Println("Can't recieve")
			break
		}

		json.Unmarshal([]byte(msg), &t)
		contents := t.Contents.(map[string]interface{})
		switch t.Type {
		case "signup":
			new_user(ws, contents)
			break
		case "login":
			if validate_login(ws, contents) {
				send_data(ws)
			}
			break
		}
	}
}

func send_data(ws *websocket.Conn) {
	var m []data_types.TestData
	resp := data_types.OutputMessage{Type: "data", Contents: ""}
	_, err := dbmap.Select(&m, "SELECT Id, UNIX_TIMESTAMP(Date) Date, X, Y from test_data")

	if err == nil {
		resp.Contents = m
		msg, _ := json.Marshal(resp)
		websocket.Message.Send(ws, string(msg))
	}
}

func validate_login(ws *websocket.Conn, contents map[string]interface{}) bool {
	var user data_types.User
	ret := false
	pass := contents["pass"].(string)
	resp := data_types.OutputMessage{Type: "login", Contents: ""}

	err := dbmap.SelectOne(&user, "SELECT * FROM users WHERE UserName=?", contents["user"].(string))
	if err == nil && bcrypt.CompareHashAndPassword([]byte(user.Pass), []byte(pass)) == nil {
		resp.Contents = user
		ret = true
	} else {
		resp.Contents = data_types.Error{Error: "User does not exist or password did not match user"}
	}
	msg, _ := json.Marshal(resp)
	websocket.Message.Send(ws, string(msg))

	return ret
}

func new_user(ws *websocket.Conn, contents map[string]interface{}) {
	var exists int
	user := contents["user"].(string)
	pass := contents["pass"].(string)

	resp := data_types.OutputMessage{Type: "signup", Contents: ""}
	err := dbmap.SelectOne(&exists, "SELECT 1 FROM users WHERE UserName=?", contents["user"].(string))
	if err == nil {
		resp.Contents = data_types.Error{Error: "That user name is already in use"}
	} else {
		hash, err := bcrypt.GenerateFromPassword([]byte(pass), 10)
		if err != nil {
			fmt.Println("Unable to hash password")
		}
		dbmap.Exec("INSERT INTO users (UserName, Pass) Values (?, ?)", user, hash)
	}

	msg, _ := json.Marshal(resp)
	websocket.Message.Send(ws, string(msg))
}
